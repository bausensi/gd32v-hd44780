/*!
    \file  main.c
    \brief Operation example of the GD32V HD44780 LCD library for a 20x4 LCD
    \version 1
*/

/*
    Copyright (c) 2020, Benjamín Ausensi Tapia.
    https://gitlab.com/insaane

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/

#include "gd32vf103.h"
#include "systick.h"
#include "hd44780.h"

#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>

void lcd_printf(struct lcd *const target, const char *format, ... ) {
    char buffer[40];
    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);

    for(uint8_t i = 0; i < 40; i++) {
        if(!buffer[i])
            return;
        lcd_write(target, (uint8_t)buffer[i], LCD_WRITE_DATA);
    }
}

int main(void) {
    static struct lcd my_lcd;
    /* Specify the size of the display in cols x rows.  Maximum cols 31, rows 5 */
    my_lcd.resolution           = LCD_EASY_RESOLUTION(20, 4);

    my_lcd.gpio_rse             = GPIOC;
    my_lcd.gpio_upper_nibble    = GPIOA;
    my_lcd.rcu_rse              = RCU_GPIOC;
    my_lcd.rcu_upper_nibble     = RCU_GPIOA;

    my_lcd.io_rs                = GPIO_PIN_15;
    my_lcd.io_rw                = GPIO_PIN_14;
    my_lcd.io_en                = GPIO_PIN_13;

    my_lcd.io_db[4]             = GPIO_PIN_4;
    my_lcd.io_db[5]             = GPIO_PIN_5;
    my_lcd.io_db[6]             = GPIO_PIN_6;
    my_lcd.io_db[7]             = GPIO_PIN_7;

    /* This has a 20x4 LCD in mind. Modify the code accordingly for your display                */
    /* http://web.alfredstate.edu/faculty/weimandn/lcd/lcd_addressing/lcd_addressing_index.html */

    lcd_init(&my_lcd);
    lcd_printf(&my_lcd, "Hello GD32VF103");
    lcd_write(&my_lcd, LCD_CMD_SETDDRAMADDR | 0x54, LCD_WRITE_COMMAND);
    lcd_printf(&my_lcd, "This is a 20x4 LCD.");
    
    uint8_t i = 0;
    uint8_t j = 0;

    while(1) {
        lcd_write(&my_lcd, LCD_CMD_SETDDRAMADDR | 0x40, LCD_WRITE_COMMAND);
        lcd_printf(&my_lcd, "Value of i: %03u", i);
        lcd_write(&my_lcd, LCD_CMD_SETDDRAMADDR | 0x14, LCD_WRITE_COMMAND);
        lcd_printf(&my_lcd, "Value of j: %03u", j);
        i++;
        j+=2;
        delay_1us(333333);
    }
}