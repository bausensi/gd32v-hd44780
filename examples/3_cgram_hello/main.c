/*!
    \file  main.c
    \brief Operation example of the GD32V HD44780 LCD library for a 20x4 LCD
    \version 1
*/

/*
    Copyright (c) 2020, Benjamín Ausensi Tapia.
    https://gitlab.com/insaane

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/

#include "gd32vf103.h"
#include "systick.h"
#include "hd44780.h"

#include <stdio.h>
#include <stdarg.h>

/* Store your data as little-endian. */
uint64_t pacman = 0x0E1F1E1C1E1B0E00;
uint64_t dot    = 0x0000000606000000;

void lcd_printf(struct lcd *const target, const char *format, ... ) {
    char buffer[40];
    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);

    for(uint8_t i = 0; i < 40; i++) {
        if(!buffer[i])
            return;
        lcd_write(target, (uint8_t)buffer[i], LCD_WRITE_DATA);
    }
}

int main(void) {
    static struct lcd my_lcd;

    my_lcd.resolution           = LCD_EASY_RESOLUTION(16, 2);
    my_lcd.interface_settings   = LCD_SETINTERFACE_8BIT;
    
    my_lcd.gpio_rse             = GPIOC;
    my_lcd.gpio_upper_nibble    = GPIOA;
    my_lcd.gpio_lower_nibble    = GPIOA;

    my_lcd.rcu_rse              = RCU_GPIOC;
    my_lcd.rcu_upper_nibble     = RCU_GPIOA;
    my_lcd.rcu_lower_nibble     = RCU_GPIOA;

    my_lcd.io_rs                = GPIO_PIN_15;
    my_lcd.io_rw                = GPIO_PIN_14;
    my_lcd.io_en                = GPIO_PIN_13;
    
    my_lcd.io_db[0]             = GPIO_PIN_0;
    my_lcd.io_db[1]             = GPIO_PIN_1;
    my_lcd.io_db[2]             = GPIO_PIN_2;
    my_lcd.io_db[3]             = GPIO_PIN_3;

    my_lcd.io_db[4]             = GPIO_PIN_4;
    my_lcd.io_db[5]             = GPIO_PIN_5;
    my_lcd.io_db[6]             = GPIO_PIN_6;
    my_lcd.io_db[7]             = GPIO_PIN_7;

    /* This has a 16x2 LCD in mind. Modify the code accordingly for your display                */
    /* http://web.alfredstate.edu/faculty/weimandn/lcd/lcd_addressing/lcd_addressing_index.html */

    lcd_init(&my_lcd);
    lcd_printf(&my_lcd, "Hello GD32VF103");
    lcd_cgram_upload(&my_lcd, pacman, 1);
    lcd_cgram_upload(&my_lcd, dot,    2);
    lcd_write(&my_lcd, LCD_CMD_SETDDRAMADDR | 0x40, LCD_WRITE_COMMAND);
    lcd_printf(&my_lcd, "\1\2\2\2\2\2CGRAM DEMO");

    while(1);
}
