#ifndef GD32V_HD44780_H
#define GD32V_HD44780_H

#include "gd32vf103.h"
#include <stdint.h>

/* Last value of lcd_write() */
#define LCD_WRITE_COMMAND 0
#define LCD_WRITE_DATA    1

/* For those who don't know how to do bit math */
#define LCD_EASY_RESOLUTION(WIDTH, HEIGHT) ((WIDTH << 3) | HEIGHT)

/* HD44780 8-bit commands */
#define LCD_CMD_CLEAR                    1 << 0
#define LCD_CMD_SETHOME                  1 << 1

#define LCD_CMD_SETCURSORDIRECTION       1 << 2
#define LCD_SETCURSORDIRECTION_INCREMENT 1 << 1
#define LCD_SETCURSORDIRECTION_SHIFTDISP 1 << 0

#define LCD_CMD_DISPLAYCURSORSETUP       1 << 3
#define LCD_DISPLAYCURSORSETUP_DISPLAYON 1 << 2
#define LCD_DISPLAYCURSORSETUP_BLINKON   1 << 1
#define LCD_DISPLAYCURSORSETUP_CURSORON  1 << 0

#define LCD_CMD_MOVEORSHIFT              1 << 4

#define LCD_CMD_SETINTERFACE             1 << 5
#define LCD_SETINTERFACE_8BIT            1 << 4
#define LCD_SETINTERFACE_2LINES          1 << 3
#define LCD_SETINTERFACE_5X10FONT        1 << 2

#define LCD_CMD_SETCGRAMADDR             1 << 6
#define LCD_CMD_SETDDRAMADDR             1 << 7

struct lcd {
    uint32_t gpio_rse;
    uint32_t rcu_rse;

    uint32_t gpio_upper_nibble;
    uint32_t rcu_upper_nibble;

    uint32_t gpio_lower_nibble;
    uint32_t rcu_lower_nibble;

    uint32_t io_rs;
    uint32_t io_rw;
    uint32_t io_en;
    uint32_t io_db[8];

    uint8_t  interface_settings;

    /* Amount of cols: upper 5 bits. Amount of rows: lower 3 bits. */
    uint8_t resolution;
};

/* Internal functions */
void lcd_internal_pulse_enable(struct lcd *const target);
void lcd_internal_write_nibble(struct lcd *const target, uint8_t data);
void lcd_internal_write_byte(struct lcd *const target, uint8_t data);

/* Functions for users */
void lcd_write(struct lcd *const target, uint8_t data, uint8_t writemode);
void lcd_init(struct lcd *const target);
void lcd_cgram_upload(struct lcd *const target, uint64_t data, uint8_t index);

#endif
