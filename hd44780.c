#include "hd44780.h"
#include "systick.h"

//Internal functions
void lcd_internal_pulse_enable(struct lcd *const target) {
    gpio_bit_write(target->gpio_rse, target->io_en, 0);
    delay_1us(1);
    gpio_bit_write(target->gpio_rse, target->io_en, 1);
    delay_1us(1);    // enable pulse must be >450ns
    gpio_bit_write(target->gpio_rse, target->io_en, 0);
    delay_1us(100);   // commands need > 37us to settle
}

void lcd_internal_write_nibble(struct lcd *const target, uint8_t data) {
    for(uint8_t i = 4; i < 8; i++)
        gpio_bit_write(target->gpio_upper_nibble, target->io_db[i], ((data >> (i - 4)) & 1));
    lcd_internal_pulse_enable(target);
}
void lcd_internal_write_byte(struct lcd *const target, uint8_t data) {
    for(uint8_t i = 0; i < 4; i++)
        gpio_bit_write(target->gpio_lower_nibble, target->io_db[i], ((data >> (i)) & 1));
    for(uint8_t i = 4; i < 8; i++)
        gpio_bit_write(target->gpio_upper_nibble, target->io_db[i], ((data >> (i)) & 1));
    lcd_internal_pulse_enable(target);
}


//Interface functions
void lcd_write(struct lcd *const target, uint8_t data, uint8_t writemode) {
    gpio_bit_write(target->gpio_rse, target->io_rs, writemode);

    if(target->interface_settings & LCD_SETINTERFACE_8BIT)
        lcd_internal_write_byte(target, data);
    else {
        lcd_internal_write_nibble(target, (data >> 4));
        lcd_internal_write_nibble(target, data);
    }
}

void lcd_init(struct lcd *const target) {
    rcu_periph_clock_enable(target->rcu_rse);
    rcu_periph_clock_enable(target->rcu_upper_nibble);

    gpio_init(target->gpio_rse, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, target->io_rs);
    gpio_init(target->gpio_rse, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, target->io_rw);
    gpio_init(target->gpio_rse, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, target->io_en);
    
    if(target->interface_settings & LCD_SETINTERFACE_8BIT) {
        rcu_periph_clock_enable(target->rcu_lower_nibble);
        for(uint8_t i = 0; i < 4; i++)
            gpio_init(target->gpio_lower_nibble, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, target->io_db[i]);
    }

    for(uint8_t i = 4; i < 8; i++)
        gpio_init(target->gpio_upper_nibble, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, target->io_db[i]);

    //x AND 7: mask upper 5 bits of x for comparison
    if(((target->resolution) & 7) < 2)
        //1 address line + 5x10 font
        target->interface_settings |= LCD_SETINTERFACE_5X10FONT;
    else
        //2 address lines + 5x8 font
        target->interface_settings |= LCD_SETINTERFACE_2LINES;    
    
    delay_1us(50000);
    gpio_bit_write(target->gpio_rse, target->io_rs, 0);
    gpio_bit_write(target->gpio_rse, target->io_rw, 0);
    gpio_bit_write(target->gpio_rse, target->io_en, 0);

    /* Set interface: 8-bit mode three times (make the display pay attention) */
    lcd_internal_write_nibble(target, 0x03);
    delay_1us(4200);
    lcd_internal_write_nibble(target, 0x03);
    delay_1us(4200);
    lcd_internal_write_nibble(target, 0x03); 
    delay_1us(150);

    /* Set interface: 4-bit mode, if 8-bit mode not requested from user */
    if(!(target->interface_settings & LCD_SETINTERFACE_8BIT))
        lcd_internal_write_nibble(target, 0x02);
    
    /* Set interface: use settings of interface_settings */
    lcd_write(target, LCD_CMD_SETINTERFACE | target->interface_settings, LCD_WRITE_COMMAND);

    /* Set cursor direction: Increment DDRAM pointer after every data write */
    lcd_write(target, LCD_CMD_SETCURSORDIRECTION | LCD_SETCURSORDIRECTION_INCREMENT, LCD_WRITE_COMMAND);

    /* Display/cursor setup: display ON, cursor ON */
    lcd_write(target, LCD_CMD_DISPLAYCURSORSETUP | LCD_DISPLAYCURSORSETUP_DISPLAYON | LCD_DISPLAYCURSORSETUP_CURSORON, LCD_WRITE_COMMAND);
    
    /* Finally, clear the display. */
    lcd_write(target, LCD_CMD_CLEAR, LCD_WRITE_COMMAND);
    delay_1us(2000);
}

void lcd_cgram_upload(struct lcd *const target, uint64_t data, uint8_t index) {
    index %= 8;

    /* Move CGRAM pointer by index times 8 */
    lcd_write(target, LCD_CMD_SETCGRAMADDR | (index << 3), LCD_WRITE_COMMAND);
    
    /* Write bytes packed in little-endian order by leftshifting by 8 and masking against first byte */
    for(uint8_t i = 0; i < 57; i += 8)
        lcd_write(target, 0xFF & (data >> i), LCD_WRITE_DATA);
}
